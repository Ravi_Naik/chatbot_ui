from django.contrib import admin

# Register your models here.
from accounts.models import  Chatbot
class ChatbotAdmin(admin.ModelAdmin):
    list_display = ['User_name','Acess_token','Validation_token','Api_key','Chatbot_name']

admin.site.register(Chatbot,ChatbotAdmin)