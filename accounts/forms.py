from django.forms import ModelForm,forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from accounts.models import Chatbot



class CreateUserForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']


class ProfileForm(UserCreationForm):
	class Meta:
		model = User
		fields = ['username', 'email', 'password1', 'password2']



class ChatbotForm(forms.ModelForm):
	class Meta:
		model=Chatbot
		fields=['Chatbot_name','Acess_token','Validation_token','Api_key']
