from django.urls import path
from  django.conf.urls import  url
from . import views


urlpatterns = [
	path('register/', views.registerPage, name="register"),
	path('login/', views.loginPage, name="login"),
	path('logout/', views.logoutUser, name="logout"),
	path('', views.home, name="home"),
	path('profile/', views.profile, name="profile"),
	path('editprofile/', views.editprofile, name="eprofile"),
	path('createchatbot/', views.createchatbot, name="cprofile"),
	path('botedit/<int:id>', views.botedit,name='botedit'),
	path('botupdate/<int:id>', views.botupdate,name='botupdate'),

	# url(r'^update/(?P<id>\d+)/$', views.update),
	path('admindelete/<int:id>/', views.admindelete, name='delete'),
	path('botdelete/<int:id>/', views.destroy,name='botdelete'),

]