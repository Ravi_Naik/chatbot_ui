from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
import os
from django.contrib.auth import authenticate, login, logout

from django.contrib import messages

from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import *
from .forms import  CreateUserForm,ProfileForm,ChatbotForm

from django.contrib.auth.models import User
from accounts.models import Chatbot
from shutil import move
from zipfile import ZipFile




def registerPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for ' + user)

                return redirect('login')

        context = {'form': form}
        return render(request, 'accounts/register.html', context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, 'accounts/login.html', context)







@login_required(login_url='login')
def editprofile(request):
    current_user=request.user
    print(current_user.id)
    id=current_user.id
    user=User.objects.get(id=id)
    form = ProfileForm()
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user.username =form.cleaned_data['username']
            user.email =form.cleaned_data['email']
            user.password1 =form.cleaned_data['password1']
            user.password2 =form.cleaned_data['password2']




            user.save()

            # form.save()
            # user = form.cleaned_data.get('username')
            # messages.success(request, 'Account was created for ' + user)

            return redirect('login')

    context = {'form': form}
    return render(request, 'profile.html',context)



# creating chat bot
def createchatbot(request):
    if request.method=='POST':
        chatbot=Chatbot()
        chatbot.User_name=request.POST.get('username')
        chatbot.Api_key=request.POST.get('api_key')
        chatbot.Acess_token=request.POST.get('as_token')
        chatbot.Validation_token=request.POST.get('vd_token')
        chatbot.Chatbot_name=request.POST.get('ch_name')
        chatbot.save()
        # Directory
        directory = request.POST.get('ch_name')
        cwd = os.getcwd()
        print("Current Directory:", cwd)
        parent_dir =cwd+ "/all_chatbots"
        path = os.path.join(parent_dir, directory)
        os.mkdir(path)
        ZipFile(cwd+'/temp.zip').extractall(cwd+'/all_chatbots/'+directory)

        return home(request)
    active1 = "active"
    return render(request,'chatbot.html',{'active1':active1})




def botedit(request, id):
    emp = Chatbot.objects.get(id=id)
    return render(request ,'edit.html',{'emp':emp})

def botupdate(request,id):
    chatbot = Chatbot.objects.get(id=id)
    oldname=chatbot.Chatbot_name
    if request.method=='POST':

        chatbot.User_name=request.POST.get('username')
        chatbot.Api_key=request.POST.get('api_key')
        chatbot.Acess_token=request.POST.get('as_token')
        chatbot.Validation_token=request.POST.get('vd_token')
        chatbot.Chatbot_name=request.POST.get('ch_name')
        chatbot.save()
        newname=chatbot.Chatbot_name
        cwd = os.getcwd()
        print("Current Directory:", cwd)
        move(cwd+'/all_chatbots/'+oldname, cwd+'/all_chatbots/'+newname)

        return redirect('/')
    return render(request,'edit.html')



def destroy(request, id):
    chatbot = Chatbot.objects.get(id=id)
    newname = chatbot.Chatbot_name
    chatbot.delete()
    cwd = os.getcwd()
    print("Current Directory:", cwd)
    os.rmdir(cwd+'/all_chatbots/'+newname)
    return redirect('/')


def admindelete(request, id):
    user = User.objects.get(id=id)
    user.delete()
    print("user")
    return redirect('/')



@login_required(login_url='login')
def home(request):
    us = Chatbot.objects.all()
    cwd = os.getcwd()
    active ="active"
    print("Current Directory:", cwd)
    return render(request,'index.html',{'us':us,'active':active})



@login_required(login_url='login')
def profile(request):
	return render(request ,'profile.html')

def logoutUser(request):
    logout(request)
    return redirect('login')

def startbot(request):

    return


